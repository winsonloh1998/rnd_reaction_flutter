# rnd_reaction_flutter

Research and develop an application that display the reaction button (e.g. Like, Angry, Love, Sad...) for the users to choose from.

## Screenshot - Login Page
![Login Page](https://user-images.githubusercontent.com/44187554/88160070-c13a0600-cc40-11ea-8f0e-847c78e8f9e8.png)

## Screenshot - Home Page
![Home Page](https://user-images.githubusercontent.com/44187554/88159775-5be61500-cc40-11ea-9074-bcb803c419b9.png)
