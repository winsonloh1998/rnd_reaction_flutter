import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './pictureframe.dart';

class Picture {
  final String urlPath;

  Picture({this.urlPath});

  factory Picture.fromJson(Map<String, dynamic> json) {
    return Picture(urlPath: json['download_url']);
  }
}

Future<List<Picture>> fetchPicture() async {
  final response =
      await http.get('https://picsum.photos/v2/list?page=2&limit=5');

  if (response.statusCode == 200) {
    var responseJson = json.decode(response.body);
    return (responseJson as List).map((p) => Picture.fromJson(p)).toList();
  } else {
    throw Exception('Failed to load the images');
  }
}

class Homepage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomepageState();
  }
}

class HomepageState extends State<Homepage> {
  Future<List<Picture>> futurePicture;

  @override
  void initState() {
    super.initState();
    futurePicture = fetchPicture();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('Homepage'),
      ),
      body: Container(
          margin: EdgeInsets.only(top: 10),
          alignment: Alignment.center,
          child: FutureBuilder<List<Picture>>(
            future: futurePicture,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data != null) {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return PictureFrame(
                            snapshot.data.elementAt(index).urlPath, index);
                    },
                  );
                }
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return CircularProgressIndicator();
            },
          )),
    ));
  }
}
