import '../DAOs/testdata_dao.dart';
import '../models/testdata.dart';

class TestDataRepo{
  final testDataDao = TestDataDao();

  Future updateTestData (TestData testData) {
    return testDataDao.updateTestData(testData);
  }

  Future getTestData (int id){
    return testDataDao.getTestData(id);
  }
}