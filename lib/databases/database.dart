import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';


class DBProvider{

  DBProvider._(); 
  static final DBProvider db = DBProvider._(); 
  static Database _database;

  Future<Database> get database async{
    if(_database != null)
      return _database;

    _database = await initDatabase();
    return _database;
  }

  initDatabase () async {
    // Get the location of our app directory. This is where files for our app,
    // and only our app, are stored. Files in this directory are deleted
    // when the app is deleted
    //Directory documentsDir = await getApplicationDocumentsDirectory();
    String path = join(await getDatabasesPath(), "testdata_database.db");

    return await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE TestData ("
            "id INTEGER PRIMARY KEY,"
            "likeCount INTEGER,"
            "loveCount INTEGER,"
            "wowCount INTEGER,"
            "hahaCount INTEGER,"
            "sadCount INTEGER,"
            "angryCount INTEGER,"
            "totalReact INTEGER"
            ")"
        );
        await db.execute("INSERT INTO TestData ("
          "'id','likeCount','loveCount','wowCount','hahaCount','sadCount','angryCount','totalReact') "
          "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          [0, 0, 0, 0, 0, 0, 0, 0]
        );
        await db.execute("INSERT INTO TestData ("
          "'id','likeCount','loveCount','wowCount','hahaCount','sadCount','angryCount','totalReact') "
          "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          [1, 0, 0, 0, 0, 0, 0, 0]
        );
        await db.execute("INSERT INTO TestData ("
          "'id','likeCount','loveCount','wowCount','hahaCount','sadCount','angryCount','totalReact') "
          "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          [2, 0, 0, 0, 0, 0, 0, 0]
        );
        await db.execute("INSERT INTO TestData ("
          "'id','likeCount','loveCount','wowCount','hahaCount','sadCount','angryCount','totalReact') "
          "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          [3, 0, 0, 0, 0, 0, 0, 0]
        );
        await db.execute("INSERT INTO TestData ("
          "'id','likeCount','loveCount','wowCount','hahaCount','sadCount','angryCount','totalReact') "
          "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          [4, 0, 0, 0, 0, 0, 0, 0]
        );
      }
    );  
  }
}