import 'dart:async';

import '../models/testdata.dart';
import '../repositories/testdata_repo.dart';

class TestDataBloc {
  final testDataRepository = TestDataRepo();
  final testDataController = StreamController<TestData>.broadcast();

  Stream<TestData> get singleTestData => testDataController.stream;

  TestDataBloc(int id){
    getTestData(id);
  }

  getTestData(int id) async {
    testDataController.sink.add(await testDataRepository.getTestData(id));
  }

  updateTestData(TestData data) async {
    await testDataRepository.updateTestData(data);
    getTestData(data.id);
  }

  dispose(){
    testDataController.close();
  }
}