import '../databases/database.dart';
import '../models/testdata.dart';

class TestDataDao{
  final dbProvider = DBProvider.db;


  //Get Specific Test Data
  getTestData (int id) async {
    final db = await dbProvider.database;
    var result = await db.query("TestData", where: "id = ?", whereArgs: [id]);
    return result.isNotEmpty ? TestData.fromMap(result.first) : null;
  }

  //Update Test Data
  updateTestData (TestData testData) async {
    final db = await dbProvider.database;
    var result = await db.update("TestData", testData.toMap(), 
                where: "id= ?", whereArgs: [testData.id]
    );
    return result;
  }
}