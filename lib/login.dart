import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './homepage.dart';

void main() => runApp(Login());

class Authentication {
  final String token;

  Authentication({this.token});

  factory Authentication.fromJson(Map<String, dynamic> json) {
    return Authentication(token: json['token']);
  }
}

Future<Authentication> loggingIn(String email, String password) async {
  final http.Response response = await http.post(
    'https://reqres.in/api/login',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{'email': email, 'password': password}),
  );

  if (response.statusCode == 200) {
    return Authentication.fromJson(json.decode(response.body));
  } else {
    return new Authentication(token: 'Invalid');
  }
}

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  final usernameController = TextEditingController()..text = 'eve.holt@reqres.in';
  final passwordController = TextEditingController()..text = 'cityslicka';
  Future<Authentication> futureAuthentication;
  bool invalidAccount = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image(width: 250, height: 250, image: AssetImage('assets/3.png')),
              SizedBox(
                height: 5,
              ),
              TextField(
                controller: usernameController,
                obscureText: false,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Username'),
              ),
              SizedBox(
                height: 5,
              ),
              TextField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Password'),
              ),
              invalidAccount ?  Text('Invalid username and password.', style: TextStyle(color: Colors.red),) : Container(),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: Builder(
                  builder: (context) => 
                  OutlineButton(
                    highlightColor: Colors.pinkAccent[400],
                    highlightedBorderColor: Colors.pinkAccent[400],
                    child: Text('Sign In'),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    onPressed: () async {
                      Authentication auth = await loggingIn(usernameController.text, passwordController.text);
                      if(auth.token == "Invalid"){
                        setState(() {
                          invalidAccount = true;
                        });
                      }else{
                        setState(() {
                          invalidAccount = false;
                        });
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Homepage()));
                      }
                    }
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

