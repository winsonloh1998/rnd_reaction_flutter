class TestData {
  int id;
  int likeCount;
  int loveCount;
  int wowCount;
  int hahaCount;
  int sadCount;
  int angryCount;
  int totalReact;

  TestData(this.id, this.likeCount, this.loveCount, this.wowCount,
      this.hahaCount, this.sadCount, this.angryCount, this.totalReact);

  Map<String, dynamic> toMap() => {
        'id': id,
        'likeCount': likeCount,
        'loveCount': loveCount,
        'wowCount': wowCount,
        'hahaCount': hahaCount,
        'sadCount': sadCount,
        'angryCount': angryCount,
        'totalReact': totalReact
      };

  factory TestData.fromMap(Map<String, dynamic> data) {
    return TestData(
        data['id'],
        data['likeCount'],
        data['loveCount'],
        data['wowCount'],
        data['hahaCount'],
        data['sadCount'],
        data['angryCount'],
        data['totalReact']);
  }
}
