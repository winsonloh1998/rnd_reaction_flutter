import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:rndflutterapp/models/testdata.dart';
//import 'package:cached_network_image/cached_network_image.dart';
 import './reaction_configuration.dart' as ReactConfig;
// import './databases/database.dart';
import './blocs/testdata_bloc.dart';
import './DAOs/testdata_dao.dart';

class PictureFrame extends StatefulWidget {
  final String urlPath;
  final int id;

  PictureFrame(this.urlPath, this.id);

  @override
  State<StatefulWidget> createState() {
    return PictureFrameState(id);
  }
}

class PictureFrameState extends State<PictureFrame> {
  TestDataBloc testDataBloc;
  int tempReactID = -1;
  bool buttonIsChecked = false;

  PictureFrameState(int id){
    testDataBloc = new TestDataBloc(id);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1),
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        margin: EdgeInsets.fromLTRB(5, 2, 5, 2),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.network(
              widget.urlPath,
              fit: BoxFit.fill,
              width: double.infinity,
              height: 250,
              loadingBuilder: (context, child, progress) {
                return progress == null ? child : LinearProgressIndicator();
              },
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  FlutterReactionButtonCheck(
                    onReactionChanged: (reaction, isChecked) async {
                      //Get the testData from DB
                      TestDataDao testDataDao = new TestDataDao();
                      TestData testData = await testDataDao.getTestData(widget.id);
                      //Update the testData to DB
                       testDataBloc.updateTestData(updateTestDataValue(
                           testData, reaction.id, isChecked, tempReactID));

                      tempReactID = reaction.id == 0 ? -1 : reaction.id;
                      buttonIsChecked = isChecked;
                    },
                    initialReaction: ReactConfig.defaultInitialReaction,
                    reactions: ReactConfig.facebookReactions,
                  ),
                  //Results
                  StreamBuilder(
                    stream: testDataBloc.singleTestData,
                    builder: (BuildContext context,
                        AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        return Container(
                          child: Row(children: <Widget>[
                            getTopThreeReactedIcon(getReactionMap(snapshot.data)),
                            SizedBox(width: 10,),
                            Text(getReactionMsg(snapshot.data.totalReact, buttonIsChecked)),
                          ])
                        );
                      }
                      return Container();
                    },
                  )
                ],
              ),
            )
          ],
        ));
  }
}

getReactionMsg(int totalReact, bool buttonIsChecked){
  if(totalReact < 1){
    return '';
  }else if (totalReact < 2){
    if(buttonIsChecked){
      return 'You liked this';
    }else{
      return 'Total $totalReact';
    }
  }else{
    if(buttonIsChecked){
      return 'You and ${totalReact - 1} others';
    }else{
      return 'Total $totalReact';
    }
  }
}

getReactionMap(TestData data){
  return {
    1 : data.likeCount,
    2 : data.loveCount,
    3 : data.wowCount,
    4 : data.hahaCount,
    5 : data.sadCount,
    6 : data.angryCount
  };
}

getTopThreeReactedIcon(Map<int, int> reactionMap){
  //Sort the list in desecending order
  var sortedKeys = reactionMap.keys.toList(growable: false)
    ..sort((key2, key1) => reactionMap[key1].compareTo(reactionMap[key2]));
  
  LinkedHashMap sortedMap = new LinkedHashMap
    .fromIterable(sortedKeys, key: (k) => k, value: (k) => reactionMap[k]);

  sortedMap.keys.toList();

  //Store a list of widget for display 3 icons at once
  List<Widget> reactIcons = new List<Widget>();
  //Only allow maximum 3 icons to be displayed and if the reactionCount is 0 then show nothing.
  for(var i = 0; i < 3 ; i++){
    reactIcons.add((sortedMap.values.toList().elementAt(i) != 0 ? 
    Image(width: 20, height: 20, image: AssetImage(getReactionIconPath(sortedMap.keys.toList().elementAt(i)))) : 
    Container()));
  }

//Return a widget with icon
  return Container(
    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
    child: Row(children: 
      reactIcons
    ,),);
}

getReactionIconPath(int reactionId){
  switch(reactionId){
    case 1:
      return 'assets/images/like_fill.png';
    case 2:
      return 'assets/images/love.png';
    case 3:
      return 'assets/images/wow.png';
    case 4:
      return 'assets/images/haha.png';
    case 5:
      return 'assets/images/sad.png';
    case 6:
      return 'assets/images/angry.png';
  }
}

TestData updateTestDataValue(
    TestData testData, int reactionId, bool isChecked, int tempReactionId) {
  switch (reactionId) {
    case 0: // Deselect 
      if(tempReactionId != -1){
        if(tempReactionId == 1){
          testData.likeCount -= 1;
        }else if(tempReactionId == 2){
          testData.loveCount -= 1;
        }else if(tempReactionId == 3){
          testData.wowCount -= 1;
        }else if(tempReactionId == 4){
          testData.hahaCount -= 1;
        }else if(tempReactionId == 5){
          testData.sadCount -= 1;
        }else if (tempReactionId == 6){
          testData.angryCount -= 1;
        }
      }
      break;
    case 1: //Select Like 
        testData.likeCount += 1;
      break;
    case 2: //Select Love 
        testData.loveCount += 1;
      break;
    case 3: //Select Wow 
        testData.wowCount += 1;
      break;
    case 4: //Select Haha 
        testData.hahaCount += 1;
      break;
    case 5: //Select Sad 
        testData.sadCount += 1;
      break;
    case 6: //Select Angry 
        testData.angryCount += 1;
      break;
  }

  //Update total react
  if (isChecked)
    testData.totalReact += 1;
  else
    testData.totalReact -= 1;

  return testData;
}
